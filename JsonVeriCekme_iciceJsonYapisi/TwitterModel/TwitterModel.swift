//
//  TwitterModel.swift
//  JsonVeriCekme_iciceJsonYapisi
//
//  Created by Yakup Caglan on 10.11.2017.
//  Copyright © 2017 Yakup Caglan. All rights reserved.
//

import Foundation

struct TwitterModel:Codable{
    //Json verisi iç içe olduğu durumda bu şekilde iç içe structlar tanımlayarak json verilerine ulaşabilirim.
    var id:Int
    var text:String
    var user:User
    
    struct User:Codable{
        var id_str:String
        var name:String
    }
}
